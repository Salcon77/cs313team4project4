package com.example.myproject;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class CellView {
	Paint p;
	Rect r;
	
	public CellView(Rect r, Paint p)
	{
		this.r = r;
		this.p = new Paint();
		int color = p.getColor();
		this.p.setColor(color);
	}
	
	public Rect getRect()
	{
		return r;
	}
	
	public Paint getPaint()
	{
		return p;
	}
	
	public void setPaint()
	{
		this.p.setColor(Color.YELLOW);
	}
}
