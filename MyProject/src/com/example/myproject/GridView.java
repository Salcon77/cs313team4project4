package com.example.myproject;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class GridView extends View{
	Paint paint = new Paint();
	int width;
	int height;
	CellView[][] grid;
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public GridView(Context context) {
		super(context);
		paint.setColor(Color.RED);
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y - 50;
		grid = new CellView[height/100][width/100];
		int leftx = 0;
		int rightx = 100;
		int topy = 0;
		int boty = 100;
		for(int i = 0; i < grid.length; i++)
		{
			for(int j = 0; j < grid[1].length; j++)
			{
				grid[i][j] = new CellView(new Rect(leftx, topy, rightx, boty), paint);
				leftx = rightx;
				rightx += 100;
			}
			topy = boty;
			boty += 100;
			leftx = 0;
			rightx = 100;
		}		
	}
	@Override
	public void onDraw(Canvas canvas) {
		for(int i = 0; i < grid.length; i++)
		{
			for(int j = 0; j < grid[1].length; j++)
			{
				canvas.drawRect(grid[i][j].getRect(), grid[i][j].getPaint());
			}
		}
	}
	@Override
	public boolean onTouchEvent(MotionEvent e) 
	{
		int x = (int) e.getX();
		int y = (int) e.getY();
		if(y/100 > grid.length-1 || x/100 > grid[1].length-1) return false;
		grid[y/100][x/100].setPaint();
		invalidate();
		return true;
	}
}
