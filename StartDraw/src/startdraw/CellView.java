package startdraw;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class CellView {
	
	protected Rect r;
	protected Paint p;
	
	public CellView(Rect r, Paint p) {
		this.r = r;
		this.p = p;
	}
	
	public Rect getRect() {
		return r;
	}
	
	public Paint getPaint() {
		return p;
	}
	
	public void setPaint() {
		p.setColor(Color.YELLOW);
	}
}
