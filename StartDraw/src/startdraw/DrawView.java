package startdraw;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;


public class DrawView extends View {
	Paint paint = new Paint();
	CellView[][] grid;
	int M = 0;
	int N = 0;
	
	public DrawView(Context context) {
		super(context);
	}
	
	public void setUpGrid(int M, int N) {
		this.M = M;
		this.N = N;
		grid = new CellView[M][N];
		int boty = 100;
		int topy = 0;
		int leftx = 0;
		int rightx = 100;
		for(int i = 0; i < M; i++) {
			for(int j = 0; j < N; j++) {
				grid[i][j] = new CellView(new Rect(leftx, topy, rightx, boty), paint);
				leftx += 100;
				rightx += 100;
			}
			leftx = 0;
			rightx = 100;
			topy += 100;
			boty += 100;
		}
		paint.setColor(Color.RED);
		invalidate();
	}
	@Override
	public void onDraw(Canvas canvas) {
		for(int i = 0; i < M-1; i++) {
			for(int j = 0; j < N-1; j++) {
				paint = grid[i][j].getPaint();
				Rect rect = grid[i][j].getRect();
				canvas.drawRect(rect, paint);
			}
		}
	}
	@Override
	public boolean onTouchEvent(MotionEvent e) {

		int x = (int) (e.getX() / 100);
		int y = (int) (e.getY() / 100);
		grid[y][x].setPaint();
		invalidate();
		return true;	
	}
}
