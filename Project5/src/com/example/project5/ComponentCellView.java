package com.example.project5;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class ComponentCellView implements CellView {
	Rect rect;
	CellViewListener listener;
	int color = Color.RED;

	public ComponentCellView(Rect rect, CellViewListener listener) {
		this.rect = rect;
		this.listener = listener; 
	}

	public int getColor() {
		return color;
	}
	@Override
	public void setColor(int color) {
		this.color = color;
		listener.update();
	}

	@Override
	public Rect getRect() {
		return rect;
	}
	

}
