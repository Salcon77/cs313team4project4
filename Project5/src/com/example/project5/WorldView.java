package com.example.project5;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

public class WorldView extends View implements CellViewListener{

	Cell[][] grid;
	Paint paint = new Paint();
	
	public WorldView(Cell[][] cells, Context context) {
		super(context);
		grid = cells;
		final int rows = cells.length;
		final int cols = cells[0].length;
		int leftx = 0;
		int rightx = 100;
		int topy = 0;
		int boty = 100;
		for (int i = 0; i < rows; i ++) {
		     for (int j = 0; j < cols; j ++) {
		    	cells[i][j].setView(new ComponentCellView(new Rect(leftx, topy, rightx, boty), this));
				leftx = rightx;
				rightx += 100;
		     }
		     topy = boty;
			 boty += 100;
			 leftx = 0;
			 rightx = 100;
		 }
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		for(int i = 0; i < grid.length; i++)
		{
			for(int j = 0; j < grid[1].length; j++)
			{
				paint.setColor(grid[i][j].getView().getColor());
				canvas.drawRect(grid[i][j].getView().getRect(), paint);
		}
	}
}

	@Override
	public void update() {
		invalidate();	
	}
}