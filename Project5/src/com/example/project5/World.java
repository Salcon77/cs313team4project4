package com.example.project5;

public interface World {
	  /**
	   * This method adds an actor to this world at the given position.
	   */
	  void addActor(Actor actor, int xpos, int ypos);
	}
