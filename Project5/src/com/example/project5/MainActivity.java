package com.example.project5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import eco.Actor;
import eco.Cell;
import eco.CellWorld;
import eco.DynamicFactory;
import eco.Factory;
import eco.World;
import eco.WorldView;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	    // create cells according to map
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y - 100;
		
	    Cell[][] cells = new Cell[height/100][width/100];
	    for (int i = 0; i < height; i ++) {
	      for (int j = 0; j < width; j ++) {
	        cells[i][j] = new ConcreteCell();
	      }
	    }

	    // create world
	    World world = new CellWorld(cells);
	    setContentView(R.layout.activity_main);

	    // create actors and populate world
	    ArrayList<Actor> actors = new ArrayList<Actor>();
	    StringTokenizer positions = new StringTokenizer(properties.getProperty(PROPERTY_ACTOR_POSITIONS));
	    
	    actors.add(actor);
	    world.addActor(actor, x, y);

	    // create world view
	    new WorldView(cells);

	    // start actors
	  //  for (Actor a : actors) {
	 //     a.start();
	   // }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
