package com.example.project5;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;


public class ConcreteCell implements Cell{

	CellView cellView;
	ArrayList<Actor> occupants;
	ArrayList<Cell> neighbors;
	
	@Override
	public void enter(Actor actor) {
		cellView.setColor(Color.YELLOW);
		//addOccupant(actor);
	}

	@Override
	public void addNeighbor(Cell cell) {
		//neighbors.add(cell);
	}

	protected void addOccupant(Actor actor) {occupants.add(actor);}
	
	protected void removeOccupant(Actor actor) {occupants.remove(actor);}
	
	@Override
	public void clickCell() {
		cellView.setColor(Color.RED);
	}

	@Override
	public List<Cell> getNeighbors() {
		return neighbors;
	}

	@Override
	public List<Actor> getOccupants() {
		return occupants;
	}

	@Override
	public void leave(Actor actor) {
		cellView.setColor(Color.RED);
	}

	@Override
	public CellView getView() {
		return cellView;
	}
	
	@Override
	public void setView(CellView cellView) {
		this.cellView = cellView;
	}

	@Override
	public Cell randomNeighbor() {
		return null;
	}

}
