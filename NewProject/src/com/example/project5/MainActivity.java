package com.example.project5;

import java.util.ArrayList;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.view.Menu;
import android.view.MotionEvent;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
public class MainActivity extends Activity {

	Cell[][] cells;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// get the width and height of the screen.
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y; //to make up for the toolbar at bottom
		
		// create cells according to map
	    cells = new Cell[height/100][width/100];
	    for (int i = 0; i < cells.length; i ++) {
	      for (int j = 0; j < cells[1].length; j ++) {
	        cells[i][j] = new ConcreteCell();
	      }
	    }
	    
	    // create world
	    World world = new CellWorld(cells);
	    setContentView(new WorldView(cells, this));
	    
	    // create actors and populate world
	    ArrayList<Actor> actors = new ArrayList<Actor>();
	    Actor actor = new Actor();
	    actors.add(actor);
	    world.addActor(actor, 1, 2);
	    

	    // start actors
	    //for (Actor a : actors) {
	    //  a.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public boolean onTouchEvent(MotionEvent e) 
	{
		int x = (int) e.getX();
		int y = (int) e.getY();
		if(y/100 > cells.length || x/100 > cells[1].length) return false;
		System.out.println("y: " + (y/100-1));
		System.out.println("x: " + x/100);
		cells[y/100 - 1][x/100].clickCell();
		return true;
	}
}
