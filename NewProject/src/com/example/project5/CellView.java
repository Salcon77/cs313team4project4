package com.example.project5;

import android.graphics.Rect;

public interface CellView {
	
	void setColor(int color);
	
	int getColor();
	
	Rect getRect();	
}
